package com.team.blg.blgterminalapp44;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.FaceRectangle;

import java.io.InputStream;
import java.util.Locale;

public class DetectTask extends AsyncTask<InputStream, String, Face[]> {
    private FaceServiceClient faceServiceClient = new FaceServiceRestClient("https://westcentralus.api.cognitive.microsoft.com/face/v1.0", "90d1fb4d96db44398634b9cbc628d99f");

    public DetectTask() {
    }

    private static Bitmap drawFaceRectanglesOnBitmap(Bitmap originalBitmap, Face[] faces) {
        Bitmap bitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.RED);
        int stokeWidth = 2;
        paint.setStrokeWidth(stokeWidth);
        if (faces != null) {
            for (Face face : faces) {
                FaceRectangle faceRectangle = face.faceRectangle;
                canvas.drawRect(
                        faceRectangle.left,
                        faceRectangle.top,
                        faceRectangle.left + faceRectangle.width,
                        faceRectangle.top + faceRectangle.height,
                        paint);
            }
        }
        return bitmap;
    }

    @Override
    protected Face[] doInBackground(InputStream... params) {
        try {
            publishProgress("Detecting...");
            Face[] result = faceServiceClient.detect(
                    params[0],
                    true,         // returnFaceId
                    false,        // returnFaceLandmarks
                    null           // returnFaceAttributes: a string like "age, gender"
            );
            if (result == null) {
                publishProgress("Detection Finished. Nothing detected");
                return null;
            }
            publishProgress(
                    String.format(Locale.ENGLISH, "Detection Finished. %d face(s) detected",
                            result.length));
            return result;
        } catch (Exception e) {
            publishProgress("Detection failed");
            return null;
        }
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected void onPostExecute(Face[] result) {

        if (result == null) return;
    }
}
