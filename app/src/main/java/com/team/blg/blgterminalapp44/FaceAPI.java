package com.team.blg.blgterminalapp44;

import android.graphics.Bitmap;
import android.widget.RelativeLayout;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.VerifyResult;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.UUID;

class FaceAPI {
    private RelativeLayout rootView;

    public FaceAPI(RelativeLayout rootView) {
        this.rootView = rootView;
    }

    public Face[] detectAndFrame(final Bitmap imageBitmap) {
        Face[] result = null;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        DetectTask detectTask = new DetectTask();
        try {
            result = detectTask.execute(inputStream).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public VerifyResult verify(ArrayList<UUID> uuids) {
        VerifyResult result = null;
        VerifyTask verifyTask = new VerifyTask(uuids);
        try {
            result = verifyTask.execute().get();
        } catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }



}
