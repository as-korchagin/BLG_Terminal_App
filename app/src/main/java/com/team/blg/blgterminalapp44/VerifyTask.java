package com.team.blg.blgterminalapp44;

import android.os.AsyncTask;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.VerifyResult;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;


public class VerifyTask extends AsyncTask<InputStream, Void, VerifyResult> {
    private ArrayList<UUID> uuids;
    private FaceServiceClient faceServiceClient =
            new FaceServiceRestClient("https://westcentralus.api.cognitive.microsoft.com/face/v1.0", "90d1fb4d96db44398634b9cbc628d99f");

    public VerifyTask(ArrayList<UUID> uuids) {
        this.uuids = uuids;
    }

    @Override
    protected VerifyResult doInBackground(InputStream... params) {
        VerifyResult result = null;
        try{
            result = faceServiceClient.verify(uuids.get(0), uuids.get(1));
        } catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
}
