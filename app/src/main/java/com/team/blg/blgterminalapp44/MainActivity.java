package com.team.blg.blgterminalapp44;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.*;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.microsoft.projectoxford.face.contract.VerifyResult;

import java.util.*;

public class MainActivity extends AppCompatActivity {
    final static String TAG = "TAG";
    final int REAR_CAMERA_ID = 0;
    final int FRONT_CAMERA_ID = 1;
    SurfaceView sv;
    SurfaceHolder holder;
    HolderCallback holderCallback;
    ImageView capture;
    Camera camera;
    private UUID currentUserUuid = null;
    private UUID passwordHolderUuid = null;
    private ProgressDialog detectionProgressDialog;
    private String birthday = null;
    private ImageView rectangle = null;
    private int[] rectPos = new int[4];
    private int passTop = 0;
    private Bitmap scaledBitmap;
    private int openedCamera = 1;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        rectPos[0] = Math.round(rectangle.getX());
        rectPos[1] = Math.round(rectangle.getY());
        rectPos[2] = Math.round(rectangle.getWidth());
        rectPos[3] = Math.round(rectangle.getHeight());
        passTop = rectPos[1] + (rectPos[3] / 2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        rectangle = (ImageView) findViewById(R.id.rectangle);
        capture = (ImageView) findViewById(R.id.capture);
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.takePicture(null, null, new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {

                        final ImageView photo = (ImageView) findViewById(R.id.photo);
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                        Matrix matrix = new Matrix();
                        if (openedCamera == REAR_CAMERA_ID) {
                            matrix.postRotate(90);
                        } else {
                            matrix.postRotate(270);
                        }

                        final Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                        if (currentUserUuid == null) {
                            scaledBitmap = Bitmap.createBitmap(rotatedBitmap, rectPos[0], rectPos[1], rectPos[2], rectPos[3]);
                        } else {
                            scaledBitmap = Bitmap.createBitmap(rotatedBitmap, rectPos[0], passTop, rectPos[2], rectPos[3] - passTop + rectPos[1]);
                        }
                        photo.setImageBitmap(scaledBitmap);
                        final TextView actionText = (TextView) findViewById(R.id.action_text);
                        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("Ваш возраст подтвержден?").setPositiveButton("Да", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case AlertDialog.BUTTON_POSITIVE:
                                        currentUserUuid = null;
                                        Toast.makeText(getBaseContext(), "Приятного просмотра фильмов 18+", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }).setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case AlertDialog.BUTTON_NEGATIVE:
                                        openedCamera = 0;
                                        initCamera();
                                        actionText.setText("Сфотографируйте свой паспорт");
                                        break;
                                }
                            }
                        });
                        final AlertDialog alertDialog = builder.create();
                        detectionProgressDialog = new ProgressDialog(MainActivity.this);
                        detectionProgressDialog.setCanceledOnTouchOutside(false);
                        detectionProgressDialog.setMessage("Processing...");
                        final FaceAPI faceApi = new FaceAPI((RelativeLayout) findViewById(R.id.root_view));
                        if (currentUserUuid == null) {
                            detectionProgressDialog.show();
                            Thread faceThread = new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        currentUserUuid = faceApi.detectAndFrame(scaledBitmap)[0].faceId;
                                        if (currentUserUuid == null) {
                                            throw new Exception();
                                        } else {
                                            MainActivity.this.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    alertDialog.show();
                                                }
                                            });
                                        }
                                    } catch (Exception e) {
                                        MainActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getBaseContext(), "Лица не найдены, повторите", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                    detectionProgressDialog.dismiss();
                                }
                            };
                            faceThread.start();
                        } else {
                            final DetectBirthday recogniseApi = new DetectBirthday(scaledBitmap, getBaseContext());
                            detectionProgressDialog.setMessage("Processing...");
                            detectionProgressDialog.show();
                            Thread detectBirthday = new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        birthday = recogniseApi.execute().get();
                                        passwordHolderUuid = faceApi.detectAndFrame(scaledBitmap)[0].faceId;
                                        ArrayList<UUID> uuids = new ArrayList<>();
                                        uuids.add(currentUserUuid);
                                        uuids.add(passwordHolderUuid);
                                        final VerifyResult result = faceApi.verify(uuids);
                                        MainActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (birthday != null & (result.confidence > 0.4)) {
                                                    Date now = new Date();
                                                    Date bdate = new Date(birthday);
                                                    long currentMills = now.getTime() - bdate.getTime();
                                                    Calendar c = Calendar.getInstance();
                                                    c.setTimeInMillis(currentMills);
                                                    int currentAge = c.get(Calendar.YEAR) - 1970;
                                                    Toast.makeText(getBaseContext(), String.valueOf(currentAge) + " год / лет", Toast.LENGTH_LONG).show();
                                                    Toast.makeText(getBaseContext(), "Возраст подтвержден", Toast.LENGTH_SHORT).show();
                                                    birthday = null;
                                                    photo.setImageDrawable(null);
                                                    actionText.setText("Сфотографируйтесь");
                                                    currentUserUuid = null;
                                                    Toast.makeText(getBaseContext(), "New session", Toast.LENGTH_SHORT).show();
                                                    openedCamera = 1;
                                                    initCamera();
                                                } else if (birthday == null) {
                                                    Toast.makeText(getBaseContext(), "Не удалось подтвердить возраст", Toast.LENGTH_SHORT).show();
                                                } else if (result.confidence > 0.4) {
                                                    Toast.makeText(getBaseContext(), "Не удалось подтвердить личность", Toast.LENGTH_SHORT).show();
                                                    Toast.makeText(getBaseContext(), String.valueOf(result.confidence), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(getBaseContext(), "Произошла ошибка, попрбуйте еще раз", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                        detectionProgressDialog.dismiss();
                                    } catch (Exception e) {
                                        Log.d(TAG, e.toString());
                                    }
                                }
                            };
                            detectBirthday.start();

                        }
                    }
                });
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        sv = (SurfaceView) findViewById(R.id.surfaceView);
        holder = sv.getHolder();
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        holderCallback = new HolderCallback();
        holder.addCallback(holderCallback);
        initCamera();

    }

    private void openFrontCamera() {
        camera = Camera.open(FRONT_CAMERA_ID);
        setCameraParams(5);
    }

    private void openRearCamera() {
        camera = Camera.open(REAR_CAMERA_ID);
        setCameraParams(12);
    }


    private void initCamera() {
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
        if (openedCamera == 0) {
            openRearCamera();
        } else {
            openFrontCamera();
        }
        if (camera != null) {

            try {
                camera.setPreviewDisplay(holder);
                setCameraDisplayOrientation(openedCamera);
            } catch (Exception e) {
                // log
            }
        }

        camera.startPreview();
    }

    private void setCameraParams(int index) {
        Camera.Parameters params = camera.getParameters();
        List<Camera.Size> sizes = params.getSupportedPictureSizes();
        params.setPictureSize(sizes.get(index).width, sizes.get(index).height);
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        camera.setParameters(params);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (camera != null)
            camera.release();
        camera = null;
    }

    void setCameraDisplayOrientation(int cameraId) {
        // определяем насколько повернут экран от нормального положения
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result = 0;

        // получаем инфо по камере cameraId
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        // задняя камера
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
            result = ((360 - degrees) + info.orientation);
        } else
            // передняя камера
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = ((360 - degrees) - info.orientation);
                result += 360;
            }
        result = result % 360;
        camera.setDisplayOrientation(result);
    }

    private class HolderCallback implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {
            camera.stopPreview();
            setCameraDisplayOrientation(openedCamera);
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

        }
    }


}
