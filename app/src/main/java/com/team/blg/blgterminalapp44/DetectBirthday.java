package com.team.blg.blgterminalapp44;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.Gson;
import com.microsoft.projectoxford.vision.VisionServiceClient;
import com.microsoft.projectoxford.vision.VisionServiceRestClient;
import com.microsoft.projectoxford.vision.contract.LanguageCodes;
import com.microsoft.projectoxford.vision.contract.OCR;
import com.microsoft.projectoxford.vision.rest.VisionServiceException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.team.blg.blgterminalapp44.MainActivity.TAG;

public class DetectBirthday extends AsyncTask<InputStream, String, String> {
    private Bitmap imageBitmap;
    private VisionServiceClient client;
    private Context context;

    public DetectBirthday(Bitmap imageBitmap, Context context) {
        this.imageBitmap = imageBitmap;
        this.context = context;
        if (client == null) {
            client = new VisionServiceRestClient(context.getString(R.string.subscription_key),
                    "https://westcentralus.api.cognitive.microsoft.com/vision/v1.0");
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }


    @Override
    protected String doInBackground(InputStream... params) {
        try {
            return process();
        } catch (Exception e) {
            Log.d(TAG, e.toString()); // Store error
        }
        return null;
    }

    @Override
    protected void onPostExecute(String data) {
        super.onPostExecute(data);
    }

    private String process() throws VisionServiceException, IOException {
        Gson gson = new Gson();

        // Put the image into an input stream for detection.
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(output.toByteArray());

        OCR ocr;
        Log.d(TAG, "start OCR");
        ocr = this.client.recognizeText(inputStream, LanguageCodes.Russian, true);
        Log.d(TAG, "stop OCR");
        String result = gson.toJson(ocr);
        Matcher rawResults = Pattern.compile("((text)\":\"([\\d.]{2,10})\"[\\]}]{0,4},)").matcher(result);
        String createdDate = "";
        while (rawResults.find()) {
            Matcher date = Pattern.compile("\"[\\d.]{1,10}\"").matcher(rawResults.group());
            while (date.find()) {
                createdDate += date.group().replaceAll("\"", "");
            }
        }
        result += "\"" + createdDate + "\"";
        ArrayList<Date> dates = new ArrayList<>();
        Matcher birthdayMatch = Pattern.compile("\\b\\d{2}\\.\\d{2}\\.\\d{4}\\b").matcher(result);
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        Date birthdate = null;
        try {
            while (birthdayMatch.find()) {
                String group = birthdayMatch.group();
                Date date = format.parse(group);
                dates.add(date);
                if (birthdate == null) {
                    birthdate = date;
                } else {
                    if (date.getTime() < birthdate.getTime()) {
                        birthdate = date;
                    }
                }

            }
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
        if (birthdate == null & createdDate.length() != 0) {
            Matcher finalMatch = Pattern.compile("[\\d]{4}").matcher(createdDate);
            if (finalMatch.find()) {
                SimpleDateFormat finalFormat = new SimpleDateFormat();
                finalFormat.applyPattern("yyyy");
                try {
                    birthdate = finalFormat.parse(finalMatch.group());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return birthdate != null ? birthdate.toString() : null;

    }
}
